use serde::Deserialize;

#[derive(Deserialize, Default, Debug)]
/// TODO
struct Splats {}

#[derive(Deserialize, Debug)]
#[serde(rename_all = "lowercase")]
pub struct Team {
    /// Name
    name: String,
    /// Score
    score: u8,
    #[serde(rename = "splats")]
    raw_splats: String,
    #[serde(skip_deserializing)]
    splats: Splats,
}
