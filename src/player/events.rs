use std::convert::TryFrom;

use enumset::{EnumSet, EnumSetType};
use serde::Deserialize;
use serde_repr::Deserialize_repr;
use thiserror::Error;

use crate::player::Team;
use crate::reader::BlobReader;

#[derive(EnumSetType, Deserialize, Debug)]
#[enumset(serialize_repr = "u8")]
pub enum Powerup {
    JukeJuice,
    RollingBomb,
    TagPro,
    TopSpeed,
}

type Powerups = EnumSet<Powerup>;

#[allow(non_snake_case)]
#[derive(Deserialize_repr, PartialEq, Clone, Copy, Debug)]
#[repr(u8)]
pub enum Flag {
    Opponent = 1,
    OpponentPotato = 2,
    Neutral = 3,
    NeutralPotato = 4,
    Temporary = 5,
}

impl Flag {
    fn try_from(x: u64) -> Option<Self> {
        match x {
            0 => None,
            1 => Some(Self::Opponent),
            2 => Some(Self::OpponentPotato),
            3 => Some(Self::Neutral),
            4 => Some(Self::NeutralPotato),
            5 => Some(Self::Temporary),
            _ => panic!("Unknown flag specifier : {}", x),
        }
    }
}

#[derive(Deserialize, Debug)]
pub enum Event {
    Join {
        team: Team,
    },
    Quit {
        flag: Option<Flag>,
        powers: Powerups,
        team: Team,
    },
    Switch {
        flag: Option<Flag>,
        powers: Powerups,
        team: Team,
    },
    Grab {
        flag: Flag,
        powers: Powerups,
        team: Team,
    },
    Capture {
        flag: Flag,
        powers: Powerups,
        team: Team,
    },
    FlaglessCapture {
        powers: Powerups,
        team: Team,
    },
    PowerUp {
        flag: Option<Flag>,
        power: Powerup,
        powers: Powerups,
        team: Team,
    },
    DuplicatePowerUp {
        flag: Option<Flag>,
        powers: Powerups,
        team: Team,
    },
    PowerDown {
        flag: Option<Flag>,
        power: Powerup,
        powers: Powerups,
        team: Team,
    },
    Return {
        flag: Option<Flag>,
        powers: Powerups,
        team: Team,
    },
    Tag {
        flag: Option<Flag>,
        powers: Powerups,
        team: Team,
    },
    Drop {
        flag: Flag,
        powers: Powerups,
        team: Team,
    },
    Pop {
        powers: Powerups,
        team: Team,
    },
    StartPrevent {
        flag: Option<Flag>,
        powers: Powerups,
        team: Team,
    },
    StopPrevent {
        flag: Option<Flag>,
        powers: Powerups,
        team: Team,
    },
    StartButton {
        flag: Option<Flag>,
        powers: Powerups,
        team: Team,
    },
    StopButton {
        flag: Option<Flag>,
        powers: Powerups,
        team: Team,
    },
    StartBlock {
        flag: Option<Flag>,
        powers: Powerups,
        team: Team,
    },
    StopBlock {
        flag: Option<Flag>,
        powers: Powerups,
        team: Team,
    },
    End {
        flag: Option<Flag>,
        powers: Powerups,
        team: Option<Team>,
    },
}

#[derive(Error, Debug)]
pub enum EventError {
    #[error("failed to decode base64 data")]
    DecodeError(#[from] base64::DecodeError),
    #[error("failed to parse blob data")]
    ReaderError(#[from] crate::reader::ReaderError),
    #[error("unknown powerup")]
    UnknownPowerup(u8),
    #[error("unexpected timestamp")]
    TimestampOverflow,
    #[error("game events with no team defined")]
    MissingTeam,
}

pub fn parse_events(
    data: Vec<u8>,
    team: Option<Team>,
    duration: u32,
) -> Result<Vec<(u32, Event)>, EventError> {
    let mut reader = BlobReader::new(data);
    let mut events = Vec::new();

    // initial game state
    let mut flag: Option<Flag> = None;
    let mut powers = Powerups::empty();
    let mut prevent = false;
    let mut button = false;
    let mut block = false;
    let mut time: u32 = 0;
    let mut team = team;

    while !reader.end() {
        // 0. Initialize chunk variables
        let (
            new_team,
            drop_pop,
            returns,
            tags,
            grab,
            captures,
            keep,
            new_flag,
            mut new_powers,
            mut powers_down,
            mut powers_up,
            toggle_prevent,
            toggle_button,
            toggle_block,
        );

        // 1. Read a chunk from the blob reader
        new_team = if reader.read_bool() {
            if let Some(team) = team {
                if reader.read_bool() {
                    None // quit
                } else {
                    Some(team.switch()) // switch
                }
            } else {
                Some(Team::from(reader.read_bool())) // join
            }
        } else {
            team // stay
        };
        drop_pop = reader.read_bool();
        returns = reader.read_tally()?;
        tags = reader.read_tally()?;
        grab = flag.is_none() && reader.read_bool();
        captures = reader.read_tally()?;
        keep = !drop_pop
            && new_team.is_some()
            && (new_team == team || team.is_none())
            && (captures == 0 || (flag.is_none() && !grab) || reader.read_bool());
        new_flag = if grab {
            if keep {
                Flag::try_from(1 + reader.read_fixed(2)?)
            } else {
                Some(Flag::Temporary)
            }
        } else {
            flag
        };
        new_powers = reader.read_tally()?;
        powers_down = Powerups::empty();
        powers_up = Powerups::empty();
        let mut i = 1;
        while i < 16 {
            let power = Powerups::from_u8(i);
            if !powers.intersection(power).is_empty() {
                if reader.read_bool() {
                    powers_down |= power;
                }
            } else if new_powers != 0 && reader.read_bool() {
                powers_up |= power;
                new_powers -= 1;
            }
            i <<= 1;
        }
        powers |= powers_up;
        powers -= powers_down;
        toggle_prevent = reader.read_bool();
        toggle_button = reader.read_bool();
        toggle_block = reader.read_bool();
        let frames = reader.read_footer()?;
        let frames = u32::try_from(frames).map_err(|_| EventError::TimestampOverflow)?;
        time = time
            .checked_add(1 + frames)
            .ok_or(EventError::TimestampOverflow)?;

        // 2. Construct events
        if team.is_none() {
            if new_team.is_some() {
                team = new_team;
                events.push((
                    time,
                    Event::Join {
                        team: team.unwrap(),
                    },
                ));
            }
        }
        let team = team.ok_or(EventError::MissingTeam)?;
        for _ in 0..returns {
            events.push((time, Event::Return { flag, powers, team }));
        }
        for _ in 0..tags {
            events.push((time, Event::Tag { flag, powers, team }))
        }
        if grab {
            flag = new_flag;
            let flag = new_flag.unwrap();
            events.push((time, Event::Grab { flag, powers, team }));
        }
        for _ in 0..captures {
            if keep || flag.is_none() {
                events.push((time, Event::FlaglessCapture { powers, team }));
            } else {
                {
                    let flag = flag.unwrap();
                    events.push((time, Event::Capture { flag, powers, team }));
                }
                flag = None;
            }
        }
        for power in powers_down.iter() {
            events.push((
                time,
                Event::PowerDown {
                    flag,
                    power,
                    powers,
                    team,
                },
            ))
        }
        for power in powers_up.iter() {
            events.push((
                time,
                Event::PowerUp {
                    flag,
                    power,
                    powers,
                    team,
                },
            ))
        }
        if toggle_prevent {
            if prevent {
                events.push((time, Event::StopPrevent { flag, powers, team }));
                prevent = false;
            } else {
                events.push((time, Event::StartPrevent { flag, powers, team }));
                prevent = true;
            }
        }
        if toggle_button {
            if button {
                events.push((time, Event::StopButton { flag, powers, team }));
                button = false;
            } else {
                events.push((time, Event::StartButton { flag, powers, team }));
                button = true;
            }
        }
        if toggle_block {
            if block {
                events.push((time, Event::StopBlock { flag, powers, team }));
                block = false;
            } else {
                events.push((time, Event::StartBlock { flag, powers, team }));
                block = true;
            }
        }
        if drop_pop {
            if flag.is_some() {
                {
                    let flag = flag.unwrap();
                    events.push((time, Event::Drop { flag, powers, team }));
                }
                flag = None;
            } else {
                events.push((time, Event::Pop { powers, team }));
            }
        }
        if new_team != Some(team) {
            if let Some(team) = new_team {
                events.push((time, Event::Switch { flag, powers, team }));
            } else {
                events.push((time, Event::Quit { flag, powers, team }));
                flag = None;
            }
        }
    }
    events.push((duration, Event::End { flag, powers, team }));
    Ok(events)
}
