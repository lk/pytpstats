use thiserror::Error;

use crate::Game;

fn get_game_url(game_id: String) -> String {
    format!("https://tagpro.eu/data/?match={}", game_id)
}

#[derive(Error, Debug)]
pub enum FetchError {
    #[error("failed to fetch JSON data from Remote server")]
    HTTPError(#[from] reqwest::Error),
    #[error("failed to parse JSON data")]
    ParseError(#[from] serde_json::Error),
}

pub fn fetch_game_data(game_id: String) -> Result<Game, FetchError> {
    Ok(reqwest::blocking::get(get_game_url(game_id))?.json()?)
}
