use serde::Deserialize;

pub mod fetcher;
pub mod map;
pub mod player;
pub mod reader;
pub mod team;
pub mod writer;

#[derive(Deserialize, Debug)]
#[serde(rename_all = "camelCase")]
pub struct Game {
    /// Game server address
    server: String,
    /// Game server port
    port: u16,
    /// Game played on official server
    official: bool,
    /// 8 character code of the game, usually not provided by the server
    group: Option<String>,
    /// UNIX Timestamp of game end
    date: u64,
    /// Maximum duration of the game, in seconds
    time_limit: u16,
    /// Game duration, in frames (60 frames per seconds)
    pub duration: u32,
    /// Whether the game was finished (reached capture/time limit)
    finished: bool,
    /// Map
    map: map::Map,
    /// Players
    pub players: Vec<player::Player>,
    /// Teams
    teams: Vec<team::Team>,
}
