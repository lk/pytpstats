use std::convert::TryFrom;
use thiserror::Error;

pub struct BlobReader {
    data: Vec<u8>,
    position: usize,
}

#[derive(Error, Debug)]
pub enum ReaderError {
    #[error("integer overflow")]
    Overflow,
}

impl BlobReader {
    pub fn new(data: Vec<u8>) -> Self {
        Self { data, position: 0 }
    }

    fn value(&self) -> u8 {
        self.data[self.position >> 3]
    }

    pub fn end(&self) -> bool {
        self.position >> 3 >= self.data.len()
    }

    pub fn read_bool(&mut self) -> bool {
        if self.end() {
            return false;
        };
        let result = self.value() >> 7 - (self.position & 7) & 1;
        self.position += 1;
        result != 0
    }

    pub fn read_fixed(&mut self, mut bits: u8) -> Result<u64, ReaderError> {
        if bits > 64 {}
        // TODO: handle overflows, check that bits < 64
        let mut result = 0;
        while bits != 0 {
            result = result << 1;
            if self.read_bool() {
                result += 1;
            }
            bits -= 1;
        }
        Ok(result)
    }

    pub fn read_tally(&mut self) -> Result<u8, ReaderError> {
        let mut result: u8 = 0;
        while self.read_bool() {
            result = result.checked_add(1).ok_or(ReaderError::Overflow)?;
        }
        Ok(result)
    }

    pub fn read_footer(&mut self) -> Result<u64, ReaderError> {
        let mut minimum = 0;
        let mut size = u8::try_from(self.read_fixed(2)? << 3).map_err(|_| ReaderError::Overflow)?;
        let mut free =
            u8::try_from(8 - (self.position & 7) & 7).map_err(|_| ReaderError::Overflow)?;
        size |= free;
        while free < size {
            minimum += 1 << free;
            free += 8
        }
        Ok(self.read_fixed(size)? + minimum)
    }
}
